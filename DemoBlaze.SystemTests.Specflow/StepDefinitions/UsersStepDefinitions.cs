﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace DemoBlaze.SystemTests.Specflow.StepDefinitions
{
    [Binding]
    public class UsersStepDefinitions
    {
        protected const string url = "https://www.demoblaze.com";
        protected IWebDriver driver;
        protected readonly ScenarioContext _scenarioContext;

        public UsersStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;

            new DriverManager().SetUpDriver(new ChromeConfig(), "MatchingBrowser");
            var options = PrepareChromeOptions();
            driver = new ChromeDriver(options);
        }

        [AfterScenario(Order = int.MaxValue)]
        public void AfterScenario()
        {
            driver.Quit();
        }

        private ChromeOptions PrepareChromeOptions()
        {
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("--incognito");
            options.AcceptInsecureCertificates = true;
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("--disable-popup-blocking");
            return options;
        }
    }
}
