﻿Feature: Users
	- signing up
	- signing in
	- checking relevant error messages


Scenario: Signing up a correct user should display a success message 
	Given a home page is open
	When signing up a new user
	Then the sign up successful message is displayed

#Scenario: Signing up an incorrect user should display an error message 
#	Given a home page is open
#	When signing up a new invalid user
#	Then the missing username or password field message is displayed

#Scenario: Signing up an existing user should display an error message 
#	Given a home page is open
#		* a new user already exists
#	When signing up an existing user
#	Then the exisint user message is displayed
